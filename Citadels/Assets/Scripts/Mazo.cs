using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Mazo : MonoBehaviour
{
    public List<Carta> lstCartas;

    public void PosicionarCarta(Carta pCarta, Transform pos)
    {
        pCarta.transform.position = pos.transform.position;
        pCarta.transform.rotation = pos.transform.rotation;
        pCarta.gameObject.transform.parent = null;
        pCarta.gameObject.transform.localScale = new Vector3(2, 0.2f, 3);
        pCarta.gameObject.transform.parent = pos.gameObject.transform;
    }
    public void AgregarCarta(Carta pCarta)
    {
        lstCartas.Add(pCarta);
    }
    public void SacarCarta(int index) // lo llama el primero en elegir personaje para sacar una 
    {
        lstCartas.Remove(lstCartas[index]);
    }
    public void SacarCarta(Carta pCarta) // lo llama el primero en elegir personaje para sacar una 
    {
        lstCartas.Remove(pCarta);
    }
    public void MostrarCarta(int index) // para scar 2 cartas del mazo, mostrarlas y esas no se usan
    {
        lstCartas[index].gameObject.transform.rotation = Quaternion.Euler(lstCartas[index].gameObject.transform.eulerAngles.x, lstCartas[index].gameObject.transform.eulerAngles.y, 0);
    }
    public void MostrarCartas()
    {
        foreach (Carta c in lstCartas)
        {
            c.gameObject.transform.rotation = Quaternion.Euler(c.gameObject.transform.eulerAngles.x, c.gameObject.transform.eulerAngles.y, 0);
        }
    }
    public void OcultarCartas()
    {
        foreach (Carta c in lstCartas)
        {
            c.gameObject.transform.rotation = Quaternion.Euler(c.gameObject.transform.eulerAngles.x, c.gameObject.transform.eulerAngles.y, 180);
        }
    }
    public void BloquearCarta(int index)
    {
        lstCartas[index].BloquearCarta();
    }
    public void DesbloquearCarta(int index)
    {
        lstCartas[index].DesbloquearCarta();
    }
    public void DesbloquearCartas()
    {
        for (int i = 0; i < lstCartas.Count; i++)
        {
            lstCartas[i].DesbloquearCarta();
        }
    }
    public Carta DameCarta(int index)
    {
        return lstCartas[index];
    }
    public void MezclarCartas(List<Carta> mazo)
    {
        int rand;
        Carta cartaAux;

        for (int i = 0; i < mazo.Count; i++)
        {
            rand = Random.Range(0, mazo.Count);
            cartaAux = mazo[rand];
            mazo[rand] = mazo[i];
            mazo[i] = cartaAux;
        }
    }
    public void DesactivarCartas()
    {
        foreach (Carta c in lstCartas)
        {
            c.gameObject.SetActive(false);
        }
    }
    public void ActivarCartas()
    {
        foreach (Carta c in lstCartas)
        {
            c.gameObject.SetActive(true);
        }
    }
    public int CantidadCartas()
    {
        return lstCartas.Count;
    }
    public void LlenarMazo(List<Carta> mazo)
    {
        foreach (Carta c in mazo)
        {
            lstCartas.Add(c);
        }
    }
    public void LimpiarMazo()
    {
        lstCartas.Clear();       
    }
}
