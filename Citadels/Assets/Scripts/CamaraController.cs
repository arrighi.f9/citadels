using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{

    public Transform[] posiciones;
    private Transform posActual;
    public float vel;

    void Start()
    {
        posActual = transform;
    }
    public void RotarCamara(Jugador jugador)
    {
        int index = jugador.numJugador;
        posActual = posiciones[index];
    }

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, posActual.position, vel * Time.deltaTime);
        Vector3 angulo = new Vector3(Mathf.Lerp(transform.rotation.eulerAngles.x, posActual.transform.rotation.eulerAngles.x, vel * Time.deltaTime),
            Mathf.Lerp(transform.rotation.eulerAngles.y, posActual.transform.rotation.eulerAngles.y, vel * Time.deltaTime),
            Mathf.Lerp(transform.rotation.eulerAngles.z, posActual.transform.rotation.eulerAngles.z, vel * Time.deltaTime));

        transform.eulerAngles = angulo;
    }
}
