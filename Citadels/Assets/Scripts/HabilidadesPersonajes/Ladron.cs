using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladron : MonoBehaviour
{
    GameManager gm;
    GMHabilidadesPersonajes gmHabilidades;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gmHabilidades = FindObjectOfType<GMHabilidadesPersonajes>();
    }
    
    public void RobarAlJugador()
    {
        gmHabilidades.jugadorEnTurno = gm.jugadorenTurno;

        if (gmHabilidades.ChequearSiPersonajeEnJuego())
        {
            gmHabilidades.VincularJugadorAlPersonaje();

            if (!ChequearPersonajeAsesinado())
            {
                gmHabilidades.jugadorEnTurno.oro += gmHabilidades.jugadorAatacar.oro;
                gmHabilidades.jugadorAatacar.oro = 0;
                gmHabilidades.jugadorAatacar.ActualizarMonedasOro();
                UIManager.instance.txtAcciones.text = " Le robaste al " + GMHabilidadesPersonajes.personajeAatacar;
            }
        }
    }

    public bool ChequearPersonajeAsesinado()
    {
        if(gmHabilidades.jugadorAatacar.muerto == true)
        {
            UIManager.instance.txtIndicaciones.text = "No se puede robar a un personaje muerto. ";
            return true;
        }
        return false;
    }

}
