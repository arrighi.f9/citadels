using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Guerrero : MonoBehaviour
{
    GameManager gm;
    GMHabilidadesPersonajes gmHabilidades;

    public GameObject BtnsDecision;
    public List<Button> btnsJugadores;
    public GameObject uiAtacarJugador;
    public bool seleccionandoDistritoPropio;
    public bool seleccionandoDistritoEnemigo;
    private Jugador jugadorGuerrero;
    public Jugador jugadorVic;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gmHabilidades = FindObjectOfType<GMHabilidadesPersonajes>();
    }

    public void AtacarJugador()
    {
        gmHabilidades.VincularJugadorAlPersonaje();

        jugadorVic = gmHabilidades.jugadorAatacar;
        MostrarCartasVictima(jugadorVic);
        seleccionandoDistritoEnemigo = true;
    }

    private void Update()
    {
        if (seleccionandoDistritoPropio || seleccionandoDistritoEnemigo)
        {
            DestruirDistrito();
        }
    }
    private void DestruirDistrito()
    {
        if (seleccionandoDistritoPropio)
        {
            if (ChequearOroParaDestruir(jugadorGuerrero.MazoCartasDistrito))
            {
                if (jugadorGuerrero.CartaSeleccionada() != null && jugadorGuerrero.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.DistritoNormal
                && jugadorGuerrero.CartaEnMiMazo(jugadorGuerrero.CartaSeleccionada()))
                {
                    if (jugadorGuerrero.oro >= (jugadorGuerrero.CartaSeleccionada().carta.valor - 1))
                    {
                        jugadorGuerrero.oro -= (jugadorGuerrero.CartaSeleccionada().carta.valor - 1);
                        DestruirCarta(jugadorGuerrero);
                        jugadorGuerrero.ActualizarMonedasOro();
                        UIManager.instance.txtAcciones.text = "Destruiste el distrito " + jugadorGuerrero.CartaSeleccionada().carta.nombre + " de tu mazo ";
                        seleccionandoDistritoPropio = false;
                    }
                }
            }
            else
            {
                gm.MostrarBtnsFase3();
            }
        }
        if (seleccionandoDistritoEnemigo)
        {
            if (ChequearOroParaDestruir(jugadorVic.MazoCartasDistrito))
            {
                if (jugadorVic.cartaPersonaje.carta.nombre == "OBISPO" && jugadorVic.muerto == false)
                {
                    UIManager.instance.txtIndicaciones.text = "No se puede atacar al obispo ";
                    PosicionarMazos(jugadorVic);
                    seleccionandoDistritoEnemigo = false;
                    gm.MostrarBtnsFase3();
                }
                if (jugadorVic.ochoDistritosConstruidos == true)
                {
                    UIManager.instance.txtIndicaciones.text = "No se puede atacar un jugador que tiene su ciudad terminada ";
                    PosicionarMazos(jugadorVic);
                    seleccionandoDistritoEnemigo = false;
                    gm.MostrarBtnsFase3();
                }
                else
                {
                    if (jugadorGuerrero.CartaSeleccionada() != null && jugadorGuerrero.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.DistritoNormal
                   && jugadorVic.CartaEnMiMazo(jugadorGuerrero.CartaSeleccionada()))
                    {
                        if (jugadorGuerrero.oro >= (jugadorGuerrero.CartaSeleccionada().carta.valor - 1))
                        {
                            jugadorGuerrero.oro -= (jugadorGuerrero.CartaSeleccionada().carta.valor - 1);
                            DestruirCarta(jugadorVic);
                            jugadorGuerrero.ActualizarMonedasOro();
                            UIManager.instance.txtAcciones.text = "Destruiste el distrito " + jugadorGuerrero.CartaSeleccionada().carta.nombre + " del " + jugadorVic.gameObject.name;
                            seleccionandoDistritoEnemigo = false;

                        }
                    }
                }
            }
            else
            {
                gm.MostrarBtnsFase3();
            }
        }
    }

    private void DestruirCarta(Jugador jugador)
    {
        gm.MostrarBtnsFase3();
        jugador.CartaSeleccionada().Desconstruir();
        jugador.MazoCartasDistrito.PosicionarCarta(jugador.CartaSeleccionada(), gm.posMazoDistritos);
        jugador.MazoCartasDistrito.SacarCarta(jugador.CartaSeleccionada());
        gm.MazoDistritos.AgregarCarta(jugador.CartaSeleccionada());
        PosicionarMazos(jugadorVic);
    }

    private void MostrarCartasVictima(Jugador j)
    {
        foreach (Jugador jugador in gm.LstJugadores)
        {
            if (jugador == j)
            {
                for (int i = 0; i < j.MazoCartasDistrito.lstCartas.Count; i++)
                {
                    j.MazoCartasDistrito.PosicionarCarta(j.MazoCartasDistrito.DameCarta(i), gm.posPersonajesTablero[i]); // uso las posiciones de los 8 personajes
                    j.MazoCartasDistrito.MostrarCarta(i);
                }
            }
            else
            {
                jugador.MazoCartasDistrito.DesactivarCartas();
            }
        }
    }
    private void PosicionarMazos(Jugador jugadorVictima)
    {
        foreach (Jugador jugador in gm.LstJugadores)
        {
            if (jugador == jugadorVictima)
            {
                for (int i = 0; i < jugadorVictima.MazoCartasDistrito.lstCartas.Count; i++)
                {
                    jugadorVictima.MazoCartasDistrito.PosicionarCarta(jugadorVictima.MazoCartasDistrito.DameCarta(i), jugadorVictima.posCartasDistritos[i]); // uso las posiciones de los 8 personajes
                    jugadorVictima.MazoCartasDistrito.MostrarCarta(i);
                }
            }
            else
            {
                jugador.MazoCartasDistrito.ActivarCartas();
            }
        }
    }

    private bool ChequearOroParaDestruir(Mazo pMazo)
    {
        for (int i = 0; i < pMazo.lstCartas.Count; i++)
        {
            if (pMazo.DameCarta(i).carta.valor <= jugadorGuerrero.oro + 1)
            {
                return true;
            }
        }
        UIManager.instance.txtIndicaciones.text = " No tienes oro para destruir ningun distrito";
        return false;
    }

    public void DestruirDistritoPropio()
    {
        UIManager.instance.txtAcciones.text = "Podes destruir un distrito propio gastando 1 de oro menos de su valor ";
        seleccionandoDistritoPropio = true;
    }
    public void DestruirDistritoEnemigo()
    {
        UIManager.instance.txtAcciones.text = "Podes destruir un distrito enemigo gastando 1 de oro menos de su valor";
    }


    public void DesactivarBtnsDecision()
    {
        jugadorGuerrero = gm.jugadorenTurno;
        BtnsDecision.SetActive(false);
    }

    public void ActivarAtaqueJugador()
    {
        uiAtacarJugador.SetActive(true);
        UIManager.instance.txtIndicaciones.text = "Selecciona a que jugador atacar";
    }
    public void DesactivarAtaqueJugador()
    {
        uiAtacarJugador.SetActive(false);
    }

    public void DesactivarBotonJugadorEnTurno()
    {
        foreach (Button boton in btnsJugadores)
        {
            if (boton.gameObject.name == jugadorGuerrero.gameObject.name)
            {
                boton.interactable = false;
            }
        }
    }
    public void ActivarBotonesJugadores()
    {
        foreach (Button boton in btnsJugadores)
        {
            boton.interactable = true;
        }
    }
}
