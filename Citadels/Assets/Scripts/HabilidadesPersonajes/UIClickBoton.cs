using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class UIClickBoton : MonoBehaviour
{
    
    public void AsignarPersonajeSeleccionado() 
    {
        GMHabilidadesPersonajes.personajeAatacar = EventSystem.current.currentSelectedGameObject.name;      
    }
}
