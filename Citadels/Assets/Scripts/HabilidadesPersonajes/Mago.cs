using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Mago : MonoBehaviour
{
    GameManager gm;
    GMHabilidadesPersonajes gmHabilidades;

    public GameObject BtnsDecision;
    public List<Button> btnsJugadores;
    public GameObject uiIntercambioJugador;
    bool IntercambioConMazo;
    public Jugador jugadorEnTurno;
    public Jugador jugadorVictima;

    public int contCartasCambiadas;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gmHabilidades = FindObjectOfType<GMHabilidadesPersonajes>();

    }

    private void Update()
    {
        if(IntercambioConMazo == true)
        {
            UIManager.instance.txtAcciones.text = "Podes cambiar " + contCartasCambiadas + " cartas con el mazo ";
            SeleccionarCartasAIntercambiar();
        }
    }
    public void DesactivarBtnsDecision()
    {
        BtnsDecision.SetActive(false);
        jugadorEnTurno = gm.jugadorenTurno;
        gmHabilidades.jugadorEnTurno = gm.jugadorenTurno;
    }

    public void ActivarIntercambioJugador()
    {
        uiIntercambioJugador.SetActive(true);
        DesactivarBotonJugadorEnTurno();
        UIManager.instance.txtIndicaciones.text = "Elige a un jugador para intercambiar el mazo de cartas entero ";

    }
    public void DesactivarIntercambioJugador()
    {
        ActivarBotonesJugadores();
        uiIntercambioJugador.SetActive(false);
    }


    public void SeleccionarCartasAIntercambiar()
    {
        if (contCartasCambiadas > 0)
        {
            if (jugadorEnTurno.CartaSeleccionada() != null && jugadorEnTurno.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.DistritoNormal
                && jugadorEnTurno.CartaEnMiMazo(jugadorEnTurno.CartaSeleccionada()))
            {
                Carta c1 = jugadorEnTurno.CartaSeleccionada();
                Carta c2 = gm.MazoDistritos.DameCarta(Random.Range(0, gm.MazoDistritos.lstCartas.Count));

                c1.Desconstruir();
                gm.MazoDistritos.AgregarCarta(c1);
                gm.MazoDistritos.PosicionarCarta(c1, gm.posMazoDistritos);
                jugadorEnTurno.MazoCartasDistrito.SacarCarta(c1);
                jugadorEnTurno.MazoCartasDistrito.AgregarCarta(c2);
                jugadorEnTurno.MazoCartasDistrito.PosicionarCarta(c2, jugadorEnTurno.posCartasDistritos[jugadorEnTurno.AsignarPosDistrito()]);
                c2.MostrarCarta();
                gm.MazoDistritos.SacarCarta(c2);
                contCartasCambiadas -= 1;
            }
        }
        else
        {
            IntercambioConMazo = false;        
        }
    }
    public void IntercambiarCartasConMazo() // con boton 
    {
        contCartasCambiadas = 0;
        IntercambioConMazo = true;
        UIManager.instance.txtIndicaciones.text = "Selecciona las cartas que queres cambiar con el mazo ";
        contCartasCambiadas = jugadorEnTurno.MazoCartasDistrito.lstCartas.Count;
        gm.MostrarBtnsFase3();
    }
    public void IntercambiarMazos()
    {
        gmHabilidades.VincularJugadorAlPersonaje();

        jugadorVictima = gmHabilidades.jugadorAatacar;

        List<Carta> mazo1 = jugadorEnTurno.MazoCartasDistrito.lstCartas;
        List<Carta> mazo2 = jugadorVictima.MazoCartasDistrito.lstCartas;


        jugadorEnTurno.MazoCartasDistrito.lstCartas = mazo2;
        jugadorVictima.MazoCartasDistrito.lstCartas = mazo1;

        jugadorEnTurno.ChequearCartasConstruidas();
        jugadorVictima.ChequearCartasConstruidas();

        jugadorEnTurno.SumarPuntosDistritos();
        jugadorVictima.SumarPuntosDistritos();

        PosicionarMazo(jugadorEnTurno);
        PosicionarMazo(jugadorVictima);
        UIManager.instance.txtAcciones.text = " Intercambiaste tu mazo con el " + jugadorVictima.gameObject.name;
    }
    private void PosicionarMazo(Jugador jugador)
    {
        for (int i = 0; i < jugador.MazoCartasDistrito.lstCartas.Count; i++)
        {
            jugador.MazoCartasDistrito.PosicionarCarta(jugador.MazoCartasDistrito.lstCartas[i], jugador.posCartasDistritos[i]);
            jugador.MazoCartasDistrito.lstCartas[i].MostrarCarta();
        }
     
    }

    public void DesactivarBotonJugadorEnTurno()
    {
        foreach (Button boton in btnsJugadores)
        {
            if(boton.gameObject.name == jugadorEnTurno.gameObject.name)
            {
                boton.interactable = false;
            }
        }
    }
    public void ActivarBotonesJugadores()
    {
        foreach (Button boton in btnsJugadores)
        {
            boton.interactable = true;           
        }
    }
}

