using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GMHabilidadesPersonajes : MonoBehaviour
{
    public GameObject uiAsesino;
    public GameObject uiLadron;
    public GameObject uiMago;
    public GameObject uiGuerrero;

    public static string personajeAatacar = "";
    public Jugador jugadorAatacar;
    public Jugador jugadorEnTurno;
    GameManager gm;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }
    public bool ChequearSiPersonajeEnJuego() 
    {
        jugadorEnTurno = gm.jugadorenTurno;
        for (int i = 0; i < gm.lstPersonajesEnJuego.Count; i++)
        {
            if (personajeAatacar == gm.lstPersonajesEnJuego[i].carta.nombre)
            {
                return true;
            }
        }
        UIManager.instance.txtIndicaciones.text = " El personaje seleccionado no esta en juego ";
        return false;
    }
    public void VincularJugadorAlPersonaje()
    {
        for (int i = 0; i < gm.LstJugadores.Count; i++)
        {
            if (gm.LstJugadores[i].cartaPersonaje.carta.nombre == personajeAatacar || gm.LstJugadores[i].gameObject.name == personajeAatacar)
            {
                jugadorAatacar = gm.LstJugadores[i];
            }
        }
    }

    public void ActivarUiAsesino()
    {
        uiAsesino.SetActive(true);
    }
    public void ActivarUiLadron()
    {
        uiLadron.SetActive(true);
    }
    public void ActivarUiMago()
    {
        uiMago.SetActive(true);
    }
    public void ActivarUiGuerrero()
    {
        uiGuerrero.SetActive(true);
    }

    public void DesactivarUiAsesino()
    {
        uiAsesino.SetActive(false);
    }
    public void DesactivarUiLadron()
    {
        uiLadron.SetActive(false);
    }
    public void DesactivarUiMago()
    {
        uiMago.SetActive(false);
    }
    public void DesactivarUiGuerrero()
    {
        uiGuerrero.SetActive(false);
    }

    public void DesactivarHabilidades()
    {
        DesactivarUiAsesino();
        DesactivarUiLadron();
        DesactivarUiMago();
        DesactivarUiGuerrero();
    }
    public void ActivarHabilidad(string personaje) // va activar cosas de la ui para que use la habilidad
    {
        switch (personaje)
        {
            case "ASESINO":
                ActivarUiAsesino();
                break;
            case "LADRON":
                ActivarUiLadron();
                break;
            case "MAGO":
                ActivarUiMago();
                break;
            case "GUERRERO":
                ActivarUiGuerrero();
                break;
        }
    }

}
