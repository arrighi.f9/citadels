using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Asesino : MonoBehaviour
{
    GameManager gm;
    GMHabilidadesPersonajes gmHabilidades;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gmHabilidades = FindObjectOfType<GMHabilidadesPersonajes>();
    }

    public void MatarPersonaje()
    {
        gmHabilidades.jugadorEnTurno = gm.jugadorenTurno;

        if (gmHabilidades.ChequearSiPersonajeEnJuego())
        {
            gmHabilidades.VincularJugadorAlPersonaje();
            if (gmHabilidades.jugadorAatacar.cartaPersonaje.cartaRey == true)
            {
                gmHabilidades.jugadorEnTurno.esRey = true;
                gmHabilidades.jugadorAatacar.esRey = false;

                gm.ChequearPasoCorona();
            }
            UIManager.instance.txtAcciones.text = " Asesinaste al " + gmHabilidades.jugadorAatacar.gameObject.name;
            gmHabilidades.jugadorAatacar.Morir();
        }
    }
}
