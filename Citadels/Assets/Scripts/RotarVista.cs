using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarVista : MonoBehaviour 
{
    public Vector3[] posiciones;
    public Vector3 posActual;

    private void Start()
    {
        posActual = transform.eulerAngles;
    }
    public void Rotar(Jugador jugador)
    {
        int index = jugador.numJugador;
        posActual = posiciones[index];
    }
    private void LateUpdate()
    {
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, posActual, 1);
    }
}
