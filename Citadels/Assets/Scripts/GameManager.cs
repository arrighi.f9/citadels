using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject camara;
    [SerializeField] private GameObject tablero;
    public Transform posMazoDistritos;
    [SerializeField] private Transform posMazoPersonajes;
    public GameObject corona;
    public GMHabilidadesPersonajes GMHabilidades;
    public GameObject Scores;

    #region VariablesTurnos

    private bool etapaSeleccionTerminada;
    private bool rondaTerminada;

    public List<Jugador> LstJugadores;
    private Stack<Jugador> turnosJugadores = new Stack<Jugador>();
    public Jugador jugadorenTurno;
    [SerializeField]
    private Jugador jugadorRey;
    [SerializeField]
    #endregion

    #region Mazos 
    public List<Carta> lstPersonajes;
    public Transform[] posPersonajesTablero;
    public Mazo MazoPersonajes; // los 8 personajes que hay 
    [SerializeField]
    private Mazo MazoPersonajesDescartados;
    public List<Carta> lstPersonajesEnJuego;

    public List<Carta> lstDistritos;
    public Transform[] posDistritos;
    public Mazo MazoDistritos;
    [SerializeField]
    Mazo MazoDistritosEnTablero; // para posicionarlos 
    #endregion

    private bool gameOver;
    List<Jugador> lstJugadoresGanadores = new List<Jugador>();

    bool enFase1 = false; 
    bool seleccionandoDistrito = false; 
    bool construyendoDistritos = false;
    bool usoHabilidad = false;
    bool construccionTerminada = false; 


    private void Start()
    {
        etapaSeleccionTerminada = true;
        rondaTerminada = true;
        MazoDistritos.LlenarMazo(lstDistritos);

        SetearEstadoInicialJugadores();
    }

    #region AsignacionCorona
    public void DarCoronaJ1()
    {
        LstJugadores[0].esRey = true;
        UIManager.instance.DesactivarBtnsCorona();
    }
    public void DarCoronaJ2()
    {
        LstJugadores[1].esRey = true;
        UIManager.instance.DesactivarBtnsCorona();
    }
    public void DarCoronaJ3()
    {
        LstJugadores[2].esRey = true;
        UIManager.instance.DesactivarBtnsCorona();
    }
    public void DarCoronaJ4()
    {
        LstJugadores[3].esRey = true;
        UIManager.instance.DesactivarBtnsCorona();
    }
    #endregion

    private void Update()
    {
        if (etapaSeleccionTerminada == false) // para la etapa de seleccion
        {
            ElegirPersonaje();            
        }
        if (rondaTerminada == false) // en la ronda de juego 
        {
            if(seleccionandoDistrito == true) // fase 1 si es que eligen agarrar carta
            {
                ElegirDistrito();
            }
            if(construyendoDistritos == true) // fase 2 si decide construir
            {
                ConstruirDistrito();
            }
        }
    }

    public void ReiniciarJuego()
    {
        UIManager.instance.ResetearUi();

        etapaSeleccionTerminada = true;
        rondaTerminada = true;
        gameOver = false;

        enFase1 = false; 
        seleccionandoDistrito = false; 
        construyendoDistritos = false;
        usoHabilidad = false;
        construccionTerminada = false;

        foreach (Jugador j in LstJugadores)
        {
            j.DevolverCartasAlMazo(MazoDistritos, posMazoDistritos);
        }

        SetearEstadoInicialJugadores();

        lstJugadoresGanadores.Clear();
    }
    public void IniciarSeleccion() // con boton 
    {
        lstPersonajesEnJuego.Clear();
        MazoPersonajes.LlenarMazo(lstPersonajes);
        AsignarCorona();
        DefinirTurnosdeSeleccion();
        AsignarPosPersonjes();
        EliminacionPersonajes();
        RotarCartasyCamara();
        UIManager.instance.btnIniciarSeleccion.gameObject.SetActive(false);
        UIManager.instance.ActivarBotonesSeleccion();
        etapaSeleccionTerminada = false;
        Scores.SetActive(false);
    }
    public void SetearEstadoInicialJugadores()
    {
        
        MazoDistritos.MezclarCartas(MazoDistritos.lstCartas);
        for (int i = 0; i < 4; i++)
        {
            foreach (Jugador jugador in LstJugadores)
            {
                jugador.ResetearValores();
                jugador.MazoCartasDistrito.AgregarCarta(MazoDistritos.DameCarta(i));
                jugador.MazoCartasDistrito.PosicionarCarta(MazoDistritos.DameCarta(i), jugador.posCartasDistritos[jugador.AsignarPosDistrito()]);
                jugador.MazoCartasDistrito.MostrarCarta(i);
                MazoDistritos.SacarCarta(i);

            }
        }
    }
    public void AsignarCorona() // con boton al inicio
    {
        foreach (Jugador j in LstJugadores)
        {
            if (j.esRey)
            {
                jugadorRey = j;
            }
        }
        PosicionarCorona(jugadorRey.posCorona);
    }
    public void PosicionarCorona(Transform posCorona)
    {
        corona.transform.position = posCorona.position;
    }
    public void RotarCartasyCamara()
    {
        camara.GetComponent<CamaraController>().RotarCamara(jugadorenTurno);
        tablero.GetComponent<RotarVista>().Rotar(jugadorenTurno);
    }
    private void DesactivarJugadores()
    {
        foreach (Jugador j in LstJugadores)
        {
            if(j != jugadorenTurno)
            {
                if (j.cartaPersonaje != null)
                {
                    j.DesactivarColPersonaje();
                }
                j.enabled = false;
            }
            else
            {
                j.enabled = true;
                if (j.cartaPersonaje != null)
                {
                    j.ActivarColPersonaje();
                }
            }
        }
    }

    #region Seleccion de Personajes
    private void AsignarPosPersonjes()
    {
        MazoPersonajes.MezclarCartas(MazoPersonajes.lstCartas);
        for (int i = 0; i < posPersonajesTablero.Length; i++)
        {
            MazoPersonajes.PosicionarCarta(MazoPersonajes.DameCarta(i), posPersonajesTablero[i]);
            MazoPersonajes.ActivarCartas();
        }
    }
    private void DescartarPersonaje(Carta pcarta, Mazo pMazo)
    {
        pMazo.lstCartas.Add(pcarta);
    }
    private void EliminacionPersonajes()
    {
        MazoPersonajes.DesbloquearCartas();
        DescartarPersonaje(MazoPersonajes.DameCarta(0), MazoPersonajesDescartados);
        MazoPersonajes.BloquearCarta(0);
        MazoPersonajes.SacarCarta(0); // elimino una carta sin que se vea 

        for (int i = 0; i < 2; i++)
        {
            int rand = Random.Range(0, MazoPersonajes.CantidadCartas());

            if (MazoPersonajes.lstCartas[rand].cartaRey == true)
            {
                i -= 1;
                Debug.Log("salio carta rey");
            }
            else
            {
                DescartarPersonaje(MazoPersonajes.DameCarta(rand), MazoPersonajesDescartados);
                MazoPersonajes.MostrarCarta(rand);
                Debug.Log("se elimino la carta " + MazoPersonajes.lstCartas[rand]);
                MazoPersonajes.BloquearCarta(rand);
                MazoPersonajes.SacarCarta(rand);
            }
        }
    }
    private void DefinirTurnosdeSeleccion()
    {
        // inicio de la ronda
        foreach (Jugador j in LstJugadores) 
        {
            if (j.esRey == false)
            {
                turnosJugadores.Push(j);
            }
        }
        turnosJugadores.Push(jugadorRey); // al ultimo el rey asi es el primero en jugar
        jugadorenTurno = turnosJugadores.Peek();
        DesactivarJugadores();
    }
    private void ElegirPersonaje()
    {
        if (TurnosTermiandos(turnosJugadores)) // relleno el stack de nuevo para poder comenzar el juego una vez terminada lka seleccion 
        {
            EtapaSeleccionTerminada();
        }
        else
        {
            if (jugadorenTurno.cartaPersonaje == null)
            {
                UIManager.instance.txtIndicaciones.text = "Selecciona un personaje ";
                if (jugadorenTurno.CartaSeleccionada() != null && jugadorenTurno.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.Personaje)
                {
                    MazoPersonajes.PosicionarCarta(jugadorenTurno.CartaSeleccionada(), jugadorenTurno.posCartaPersonaje);
                    jugadorenTurno.cartaPersonaje = jugadorenTurno.CartaSeleccionada();
                    MazoPersonajes.SacarCarta(jugadorenTurno.cartaPersonaje);
                    UIManager.instance.btnTerminarTurnoSeleccion.interactable = true;
                    UIManager.instance.txtAcciones.text = "Seleccionaste al " + jugadorenTurno.CartaSeleccionada().carta.nombre;
                    lstPersonajesEnJuego.Add(jugadorenTurno.CartaSeleccionada());
                }
            }
        }
    }
    public void MostrarCartasPersonajes()
    {
        MazoPersonajes.MostrarCartas();
        UIManager.instance.btnMostrarCartasPersonaje.interactable = false;
    } // con boton 
    private void EtapaSeleccionTerminada() // limpio los mazos 
    {
        etapaSeleccionTerminada = true;
        DescartarPersonaje(MazoPersonajes.DameCarta(0), MazoPersonajesDescartados);

        MazoPersonajesDescartados.DesactivarCartas();
        MazoPersonajesDescartados.LimpiarMazo();
        MazoPersonajes.LimpiarMazo();

        UIManager.instance.DesactivarBotonesSeleccion();
        UIManager.instance.btnIniciarRonda.gameObject.SetActive(true);
        UIManager.instance.txtAcciones.text = "";
        UIManager.instance.txtIndicaciones.text = "";
        Debug.Log("Seleccion de personajes termianda");
    }
    public void TerminarTurnoSeleccion()
    {
        MazoPersonajes.OcultarCartas();
        jugadorenTurno.cartaPersonaje.OcultarCarta();
        UIManager.instance.btnMostrarCartasPersonaje.interactable = true;
        UIManager.instance.btnTerminarTurnoSeleccion.interactable = false;
        TerminarTurno();
    } // con boton 
    #endregion

    private void GuardarCartasEnMazo(Mazo mazo, Transform pos)
    {
        foreach (Carta c in mazo.lstCartas)
        {
            mazo.PosicionarCarta(c, pos);
        }
    }
    private void TerminarTurno()
    {
        UIManager.instance.txtAcciones.text = "";
        jugadorenTurno.DesactivarZoom();
        turnosJugadores.Pop();
        if (turnosJugadores.Count > 0) { jugadorenTurno = turnosJugadores.Peek(); } // que el jugador sea el de la posicion mas arriba del stack

        if (etapaSeleccionTerminada == true)
            jugadorenTurno.cartaPersonaje.MostrarCarta();                               
            
        RotarCartasyCamara();
        DesactivarJugadores();
    }
    private bool TurnosTermiandos(Stack<Jugador> turnos)
    {
        if(turnos.Count == 0)
        {
            return true;
        }
        return false;
    }

    #region Ronda de Juego
    public void ChequearPasoCorona()
    {
        if (jugadorenTurno.cartaPersonaje.cartaRey && jugadorenTurno.muerto == false)
        {
            jugadorenTurno.esRey = true;
            jugadorRey = jugadorenTurno;
            PosicionarCorona(jugadorRey.posCorona);
            foreach (Jugador j in LstJugadores)
            {
                if(j != jugadorRey)
                {
                    j.esRey = false;
                }
            }
        }
    }
    private void DefinirTurnosRonda()
    {
        LstJugadores.Sort(new ComparadorValor());
        foreach (Jugador j in LstJugadores)
        {
            turnosJugadores.Push(j);
        }
        jugadorenTurno = turnosJugadores.Peek();
    }
    public void IniciarRonda()
    {
        UIManager.instance.btnIniciarRonda.gameObject.SetActive(false);
        UIManager.instance.btnTerminarTurnoRonda.gameObject.SetActive(true);
        UIManager.instance.btnTerminarTurnoRonda.interactable = false;
        DefinirTurnosRonda();
        DesactivarJugadores();
        jugadorenTurno.cartaPersonaje.MostrarCarta();
        jugadorenTurno.ReclamarOro();
        RotarCartasyCamara();
        rondaTerminada = false;
        ChequearPasoCorona();

        MostrarBtnsFase1();
    } // con boton


    private void MostrarBtnsFase1() // para decidir si agarrar oro o carta 
    {
        if (jugadorenTurno.muerto == false)
        {
            UIManager.instance.txtIndicaciones.text = "Elige entre las dos opciones ";
            UIManager.instance.ActivarBtnsFase1();
            enFase1 = true;
        }
        else
        {
            TerminarTurnoRonda();
        }
    }
    private void MostrarBtnsFase2() // para decidir si construir o usar habilidad 
    {
        UIManager.instance.txtIndicaciones.text = "";

        if (jugadorenTurno.cartaPersonaje.carta.nombre == "REY" || jugadorenTurno.cartaPersonaje.carta.nombre == "OBISPO" ||
        jugadorenTurno.cartaPersonaje.carta.nombre == "MERCADER" || jugadorenTurno.cartaPersonaje.carta.nombre == "ARQUITECTO")
        {
            UIManager.instance.pjSinHabilidad = true;
        }
        UIManager.instance.ActivarBtnsFase2();
    }
    public void MostrarBtnsFase3() // para decidir si usar lo que queda de la fase 2 
    {
        if (construccionTerminada == false)
        {
            UIManager.instance.ActivarBtnConstruccion();
        }
        else
        {
            if(usoHabilidad == false)
            {
                UIManager.instance.ActivarBtnUsarHabilidad();
            }
            else
            {
                TerminoFase3();
            }
        }
    }
    public void TerminoFase1()
    {
        seleccionandoDistrito = false;
        enFase1 = false;
        GuardarCartasEnMazo(MazoDistritos, posMazoDistritos);
        MazoDistritosEnTablero.LimpiarMazo();
        MostrarBtnsFase2();
    }
    public void TerminoConstruccion()
    {
        UIManager.instance.DesactivarBtnsFase2();
        construyendoDistritos = false;
        construccionTerminada = true;

        if(jugadorenTurno.cartaPersonaje.carta.nombre == "REY" || jugadorenTurno.cartaPersonaje.carta.nombre == "OBISPO" ||
            jugadorenTurno.cartaPersonaje.carta.nombre == "MERCADER" || jugadorenTurno.cartaPersonaje.carta.nombre == "ARQUITECTO")
        {
            TerminoFase3();
        }
        else
        {
            MostrarBtnsFase3();
        }
    } // con boton si decide no construir nada
    public void TerminoFase3() // con boton si decide no usar la habilidad
    {
        UIManager.instance.btnTerminarTurnoRonda.interactable = true;      
    }

    public void DarOro() // con boton 
    {
        if (jugadorenTurno.cartaPersonaje.carta.nombre == "MERCADER")
        {
            UIManager.instance.txtAcciones.text = " Recibiste 3 de oro ";
            jugadorenTurno.oro += 3;
        }
        else
        {
            UIManager.instance.txtAcciones.text = " Recibiste 2 de oro ";
            jugadorenTurno.oro += 2;
        }
        jugadorenTurno.ActualizarMonedasOro();
        UIManager.instance.DesactivarBtnsFase1();
        TerminoFase1();
    }
    public void MostrarDistritosTablero()
    {
        seleccionandoDistrito = true;
        if (jugadorenTurno.cartaPersonaje.carta.nombre == "ARQUITECTO") // si es el arquitecto 
        {
            for (int i = 0; i < 5; i++)
            {
                UIManager.instance.txtIndicaciones.text = "Debes elegir 3 distritos para tu mazo ";
                MazoDistritos.PosicionarCarta(MazoDistritos.DameCarta(i), posDistritos[i]);
                MazoDistritos.MostrarCarta(i);
                MazoDistritosEnTablero.AgregarCarta(MazoDistritos.DameCarta(i));
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                UIManager.instance.txtIndicaciones.text = "Elige uno de los dos distritos para tu mazo";
                MazoDistritos.PosicionarCarta(MazoDistritos.DameCarta(i), posDistritos[i + 1]);
                MazoDistritos.MostrarCarta(i);
                MazoDistritosEnTablero.AgregarCarta(MazoDistritos.DameCarta(i));
            }
        }
        UIManager.instance.DesactivarBtnsFase1();
    } // con boton
    public void ActivarConstruccion()
    {
        UIManager.instance.txtIndicaciones.text = " Selecciona una carta de tu mazo para costruirla ";
        UIManager.instance.txtAcciones.text = "";
        UIManager.instance.DesactivarBtnsFase2();
        UIManager.instance.DesactivarBtnConstruccion();
        construyendoDistritos = true;
    }

    private void ElegirDistrito()
    {
        if (TurnosTermiandos(turnosJugadores)) // relleno el stack de nuevo para poder comenzar el juego una vez terminada lka seleccion 
        {
            RondaDeJuegoTerminada();
        }
        else
        {
            if (jugadorenTurno.MazoCartasDistrito.lstCartas.Count < jugadorenTurno.tamaņoMazo) // si tengo espacio en el mazo 
            {
                if (jugadorenTurno.CartaSeleccionada() != null && jugadorenTurno.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.DistritoNormal) // si seleccione un distrito
                {
                    jugadorenTurno.MazoCartasDistrito.AgregarCarta(jugadorenTurno.CartaSeleccionada());
                    jugadorenTurno.MazoCartasDistrito.PosicionarCarta(jugadorenTurno.CartaSeleccionada(), jugadorenTurno.posCartasDistritos[jugadorenTurno.AsignarPosDistrito()]);
                    jugadorenTurno.CartaSeleccionada().MostrarCarta();
                    MazoDistritos.SacarCarta(jugadorenTurno.CartaSeleccionada());
                    MazoDistritosEnTablero.SacarCarta(jugadorenTurno.CartaSeleccionada());

                    if (jugadorenTurno.cartaPersonaje.carta.nombre == "ARQUITECTO") // veo si soy arquitecto
                    {
                        jugadorenTurno.distritosAgarrados += 1;
                        UIManager.instance.txtAcciones.text = "Elegiste el distrito " + jugadorenTurno.CartaSeleccionada().carta.nombre;
                        if (jugadorenTurno.distritosAgarrados == 3)
                        {
                            UIManager.instance.txtAcciones.text = "Ya elegiste tus 3 distritos";
                            TerminoFase1();
                        }
                        if (jugadorenTurno.MazoCartasDistrito.lstCartas.Count == jugadorenTurno.tamaņoMazo)
                        {
                            UIManager.instance.txtIndicaciones.text = "Ya no tenes espacio en tu mazo";
                            TerminoFase1();
                        }
                        return;
                    }
                    UIManager.instance.txtAcciones.text = "Elegiste el distrito " + jugadorenTurno.CartaSeleccionada().carta.nombre;
                    if (jugadorenTurno.cartaPersonaje.carta.nombre == "MERCADER")
                    {
                        jugadorenTurno.oro += 1;
                        TerminoFase1();
                        return;
                    }
                    TerminoFase1();
                }
            }
            else
            {
                UIManager.instance.txtIndicaciones.text = "No tenes espacio en tu mazo";
                TerminoFase1();
            }
        }
    }
    private void ConstruirDistrito()
    {
        if (TurnosTermiandos(turnosJugadores)) // relleno el stack de nuevo para poder comenzar el juego una vez terminada lka seleccion 
        {
            RondaDeJuegoTerminada();
        }
        else
        {
            if (jugadorenTurno.TengoOroParaConstruir())
            {
                if (jugadorenTurno.CartaSeleccionada() != null && jugadorenTurno.CartaSeleccionada().tipoDeCarta == CartaScriptableObject.TipoCarta.DistritoNormal) // primero veo si se selecciono carta
                {
                    if (jugadorenTurno.ChequearCartaAconstruir(jugadorenTurno.CartaSeleccionada())) // si esta en mi mazo y no se construyo
                    {
                        jugadorenTurno.ConstruirDistrito(jugadorenTurno.CartaSeleccionada());

                        if (jugadorenTurno.cartaPersonaje.carta.nombre == "ARQUITECTO")
                        {
                            jugadorenTurno.distritosConstruidos += 1;
                            if (jugadorenTurno.distritosConstruidos == 3)
                            {
                                UIManager.instance.txtIndicaciones.text = "Ya no puedes construir mas distritos";
                                ChequearGanador();
                                TerminoConstruccion(); 
                            }
                            if (jugadorenTurno.ochoDistritosConstruidos)
                            {
                                ChequearGanador();
                            }
                            return;
                        }
                        else
                        {
                            ChequearGanador();
                            TerminoConstruccion();
                        }
                    }
                }
            }
            else
            {
                UIManager.instance.txtIndicaciones.text = "No tenes oro para construir ningun distrito ";
                TerminoConstruccion();
            }       
        }
    }
    public void ActivarHabilidad() // con boton para mostrar ui en base a la habilidad
    {
        usoHabilidad = true;
        UIManager.instance.DesactivarBtnUsarHabilidad();
        UIManager.instance.txtIndicaciones.text = "";
        UIManager.instance.txtAcciones.text = "";
        if (jugadorenTurno.muerto == false)
        {
            GMHabilidades.ActivarHabilidad(jugadorenTurno.cartaPersonaje.carta.nombre);
        }
    }

    public void TerminarTurnoRonda()
    {

        UIManager.instance.pjSinHabilidad = false;
        usoHabilidad = false;
        construccionTerminada = false;

        UIManager.instance.txtIndicaciones.text = "";
        UIManager.instance.txtAcciones.text = "";
        GMHabilidades.DesactivarHabilidades();
        jugadorenTurno.cartaPersonaje.OcultarCarta();
        TerminarTurno();
        ChequearPasoCorona();
        jugadorenTurno.ReclamarOro();

        if (TurnosTermiandos(turnosJugadores)) 
        {
            RondaDeJuegoTerminada();
            return;
        }
        MostrarBtnsFase1();
        ChequearPasoCorona(); // para ver si el personaje esta muerto se3 pase la corona al asesino 
        UIManager.instance.btnTerminarTurnoRonda.interactable = false;
    } 
    private void RondaDeJuegoTerminada()
    {
        if (gameOver == true)
        {
            TerminoJuego();
            UIManager.instance.btnTerminarTurnoRonda.gameObject.SetActive(false);
        }
        else
        {
            rondaTerminada = true;
            MostrarScores();
            UIManager.instance.btnTerminarTurnoRonda.gameObject.SetActive(false);
            UIManager.instance.btnIniciarSeleccion.gameObject.SetActive(true);
            GuardarCartasEnMazo(MazoPersonajes, posMazoPersonajes);

            foreach (Jugador j in LstJugadores)
            {
                j.ResetearValoresRondaDeJuego();
            }
        }
    }

    public void MostrarScores()
    {
        LstJugadores.Sort(new ComparadorPuntos());
        Scores.SetActive(true);
        for (int i = 0; i < LstJugadores.Count; i++)
        {
            LstJugadores[i].SumarPuntosDistritos();
            UIManager.instance.puntosJugadores[i].text = LstJugadores[i].gameObject.name + " = " + LstJugadores[i].puntos;
        }
        
    }
    #endregion

    public void TerminoJuego()
    {
        SumarPuntosGanadores();
        LstJugadores.Sort(new ComparadorPuntos());
        UIManager.instance.MostrarScores(LstJugadores);
    }
    public void ChequearGanador()
    {
        if (jugadorenTurno.ochoDistritosConstruidos)
        {
            lstJugadoresGanadores.Add(jugadorenTurno);
            gameOver = true;
        }
    }
    public void SumarPuntosGanadores()
    {
        lstJugadoresGanadores[0].puntos += 4;
        lstJugadoresGanadores[0].puntosBonus += 4;
        lstJugadoresGanadores[0].primerGanador = true;
        for (int i = 1; i < lstJugadoresGanadores.Count; i++)
        {
            lstJugadoresGanadores[i].puntos += 2;
            lstJugadoresGanadores[i].puntosBonus += 2;
        }        
    }
}
