using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Carta : MonoBehaviour
{
    public CartaScriptableObject.TipoCarta tipoDeCarta;
    public CartaScriptableObject carta;

    public TMP_Text txtInformacion;
    public int colorIndex; // para cehquear los colores de distritos
    public SpriteRenderer diseņoFrente;
    public SpriteRenderer diseņoReverso;

    public Sprite[] reverso;
    public Sprite spriteBloqueada;
    public Color colorResaltada;

    public SpriteRenderer construccion;
    public Sprite[] imgConstruccion;
    public bool construida;

    public bool cartaRey;
    public bool bloqueada;

    private void Awake()
    {
        SetearValores();
    }

    public void SetearValores()
    {
        diseņoFrente.sprite = carta.diseņo;
        tipoDeCarta = carta.tipoCarta;
        txtInformacion.text = carta.informacion;
        colorIndex = carta.colorIndex;
    }
    public void BloquearCarta()
    {
        diseņoFrente.sprite = spriteBloqueada;
        diseņoReverso.sprite = reverso[1];
        bloqueada = true;
    }
    public void DesbloquearCarta()
    {
        diseņoFrente.sprite = carta.diseņo;
        diseņoReverso.sprite = reverso[0];
        bloqueada = false;
    }

    public void ResaltarCarta()
    {
        diseņoFrente.color = colorResaltada;
        diseņoReverso.color = colorResaltada;
        Debug.Log(diseņoFrente.color);
    }
    public void ResetearColorCarta()
    {
        diseņoFrente.color = new Color(255, 255, 255, 255);
        diseņoReverso.color = new Color(255, 255, 255, 255);
    }
    public void OcultarCarta()
    {
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, 180);
    }
    public void MostrarCarta()
    {
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, 0);
    }

    public void Construir()
    {
        construida = true;
        construccion.sprite = imgConstruccion[1];
    }
    public void Desconstruir()
    {
        construida = false;
        construccion.sprite = imgConstruccion[0];
    }

    private void OnMouseEnter()
    {
        ResaltarCarta();
    }
  
    void OnMouseExit()
    {
        ResetearColorCarta();
    }
}
