using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " New Card", menuName = "Carta")]
public class CartaScriptableObject : ScriptableObject
{
    public enum TipoCarta { Personaje, DistritoNormal, DistritoEspecial }
    public TipoCarta tipoCarta;
    public int valor;
    public int colorIndex; // azul rojo amarillo verde gris
    public string nombre;
    public string informacion;
    public Sprite dise�o;
}
