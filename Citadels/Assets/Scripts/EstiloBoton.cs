using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class EstiloBoton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public TMP_FontAsset boldFont;
    public TMP_FontAsset normalFont;

    private TMP_Text buttonText;

    public float fadeDuration = 2f;
    private float timer;


    public float time;
    public float startTime;
    bool activo = false;

    void Start()
    {
        buttonText = GetComponentInChildren<TextMeshProUGUI>();
        buttonText.font = normalFont;
        timer = 0f;
        Menu.startGame += OcultarTexto;
    }
    private void Update()
    {

        if (!activo) { return; }

        timer += Time.deltaTime;

        // Calcular el valor de alpha para desvanecer gradualmente el texto
        float alpha = 0f - Mathf.Clamp01(timer / fadeDuration);

        buttonText.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, alpha);

        // Desactivar el GameObject cuando el texto est� completamente desvanecido
        if (alpha <= -1f)
        {
            gameObject.SetActive(false);
        }
    }
    public void OcultarTexto()
    {
        activo = true;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        buttonText.font = boldFont;
        buttonText.fontSize = 90;

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonText.font = normalFont;
        buttonText.fontSize = 75;

    }
}
