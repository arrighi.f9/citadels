using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class Jugador : MonoBehaviour
{
    [SerializeField]
    private Camera cam;

    public Material[] materiales;
    public bool muerto;
    public int puntos;
    public bool esRey;
    public int oro;
    public bool primerGanador; 

    public Transform[] posCartasDistritos;
    public Mazo MazoCartasDistrito;
    public List<Carta> cartasConstruidas; // para chequear que no haya dos iguales y la win  
    public Transform posCartaPersonaje;
    public Transform posCorona;
    public Carta cartaPersonaje;

    public int distritosAgarrados = 0; // para el arquitecto
    public int distritosConstruidos = 0; // en el turno 
    public bool ochoDistritosConstruidos;
    public string puntosDistritos = "";
    public string puntosBonus = "";

    private bool cartaenZoom;
    [SerializeField]
    private GameObject cartaZoomPersonaje;
    [SerializeField]
    private GameObject cartaZoomDistrito;

    public int numJugador; // para rotar la camara

    public int tamaņoMazo;

    public SpriteRenderer[] imgJugador;

    public GameObject[] monedas;
    public void ActualizarMonedasOro()
    {
        foreach (GameObject moneda in monedas)
        {
            moneda.SetActive(false);
        }
        for (int i = 0; i < oro; i++)
        {
            monedas[i].SetActive(true);
        }
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (cartaenZoom == false)
            {
                CartaSeleccioandaZoom();
            }
            else
            {
                DesactivarZoom();
            }
        }
    }

    public void ResetearValores()
    {
        cartaPersonaje = null;
        distritosAgarrados = 0;
        distritosConstruidos = 0;
        muerto = false;
        imgJugador[0].sharedMaterial = materiales[0];
        imgJugador[1].sharedMaterial = materiales[0];
        puntos = 0;
        primerGanador = false;
        oro = 2;
        ochoDistritosConstruidos = false;
        cartasConstruidas.Clear();
        puntosDistritos = "";
        ActualizarMonedasOro();
    }
    public void ResetearValoresRondaDeJuego()
    {
        ActivarColPersonaje();
        cartaPersonaje = null;
        distritosAgarrados = 0;
        distritosConstruidos = 0;
        muerto = false;
        imgJugador[0].sharedMaterial = materiales[0];
        imgJugador[1].sharedMaterial = materiales[0];
        ActualizarMonedasOro();
    }

    public void DevolverCartasAlMazo(Mazo mazoDistritos, Transform posMazo)
    {
        int cont = MazoCartasDistrito.lstCartas.Count;
        for (int i = 0; i < cont; i++)
        {
            MazoCartasDistrito.DameCarta(0).Desconstruir();
            MazoCartasDistrito.PosicionarCarta(MazoCartasDistrito.DameCarta(0), posMazo);
            mazoDistritos.AgregarCarta(MazoCartasDistrito.DameCarta(0));
            MazoCartasDistrito.SacarCarta(MazoCartasDistrito.DameCarta(0));
        }
    }
    public Carta CartaSeleccionada() // con click izquierdo 
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        GameObject objectHit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                objectHit = hit.collider.gameObject;
                if (objectHit.tag == "carta" && objectHit != cartaZoomPersonaje)
                {
                    if(objectHit.GetComponent<Carta>().bloqueada == true)
                    {
                        UIManager.instance.txtAcciones.text = " La carta seleccionada esta bloqueada ";
                    }
                    else
                    {
                        return objectHit.GetComponent<Carta>();
                    }                   
                }
            }
        }
        return null;
    }
    public void CartaSeleccioandaZoom() // con click derecho 
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        GameObject objectHit;
        if (Input.GetMouseButtonDown(1))
        {
            if (Physics.Raycast(ray, out hit))
            {
                objectHit = hit.collider.gameObject;
                if (objectHit.tag == "carta")
                {
                    if (objectHit.GetComponent<Carta>().bloqueada == true)
                    {
                        UIManager.instance.txtAcciones.text = " La carta seleccionada esta bloqueada ";
                    }
                    else
                    {
                        if (objectHit.GetComponent<Carta>().tipoDeCarta == CartaScriptableObject.TipoCarta.Personaje)
                        {
                            Carta cZoom = cartaZoomPersonaje.GetComponent<Carta>();
                            cZoom.carta = objectHit.GetComponent<Carta>().carta;
                            cZoom.SetearValores();                            
                            cartaZoomPersonaje.SetActive(true);
                        }
                        else
                        {
                            Carta cZoom = cartaZoomDistrito.GetComponent<Carta>();
                            cZoom.carta = objectHit.GetComponent<Carta>().carta;
                            cZoom.SetearValores();
                            cZoom.construccion.sprite = objectHit.GetComponent<Carta>().construccion.sprite; 
                            cartaZoomDistrito.SetActive(true);
                        }
                        cartaenZoom = true;
                    }
                }
            }
        }
    }

    public int AsignarPosDistrito()
    {
        for (int i = 0; i < posCartasDistritos.Length; i++)
        {
            if (posCartasDistritos[i].childCount == 0)
            {
                return i;
            }
        }
        return 7;
    }
    public void DesactivarZoom()
    {
        cartaZoomPersonaje.SetActive(false);
        cartaZoomDistrito.SetActive(false);
        cartaenZoom = false;
    }
    public void ActivarColPersonaje()
    {
        cartaPersonaje.GetComponent<BoxCollider>().enabled = true;
    }
    public void DesactivarColPersonaje()
    {
        cartaPersonaje.GetComponent<BoxCollider>().enabled = false;
    }

    public void ReclamarOro()
    {
        foreach (Carta c in MazoCartasDistrito.lstCartas)
        {
            if (cartaPersonaje.carta.colorIndex == c.carta.colorIndex)
            {
                oro += 1;
            }
        }
        ActualizarMonedasOro();
    }
    public bool ChequearCartaAconstruir(Carta pCarta)
    {
        if (CartaEnMiMazo(pCarta) && pCarta.construida == false && ChequearDistritosIguales(pCarta) && ChequearOroParaConstruirDistrito(pCarta))
        {
            return true;
        }
        return false;
    }
    public bool CartaEnMiMazo(Carta pCarta)
    {
        for (int i = 0; i < MazoCartasDistrito.lstCartas.Count; i++)
        {
            if (pCarta == MazoCartasDistrito.DameCarta(i))
            {
                return true;
            }
        }
        UIManager.instance.txtIndicaciones.text = "La carta no esta en tu mazo ";
        return false;
    }
    private bool ChequearDistritosIguales(Carta pCarta)
    {
        ChequearCartasConstruidas(); // no es lo mas performante
        for (int i = 0; i < cartasConstruidas.Count; i++)
        {
            if (pCarta.carta.nombre == cartasConstruidas[i].carta.nombre)
            {
                UIManager.instance.txtAcciones.text = "No se puede construir dos distritos con el mismo nombre ";
                return false;
            }
        }
        return true;
    }
    public void ChequearCartasConstruidas() // cuando el mago cambia mazo para actualizar la lista
    {
        cartasConstruidas.Clear();
        for (int i = 0; i < MazoCartasDistrito.lstCartas.Count; i++)
        {
            if (MazoCartasDistrito.lstCartas[i].construida == true)
            {
                cartasConstruidas.Add(MazoCartasDistrito.lstCartas[i]);
            }
        }
    }
    private bool ChequearOroParaConstruirDistrito(Carta pCarta)
    {
        if (oro >= pCarta.carta.valor)
        {
            return true;
        }
        else
        {
            UIManager.instance.txtIndicaciones.text = "No te alcanza el oro para construir " + pCarta.carta.nombre;
            return false;
        }
    }
    public void ConstruirDistrito(Carta pCarta)
    {
        pCarta.Construir();
        oro -= pCarta.carta.valor;
        cartasConstruidas.Add(pCarta);
        SumarPuntosDistritos();
        ChequearWin();
        UIManager.instance.txtAcciones.text = "Construiste el distrito " + pCarta.carta.nombre + " y te quedan " + oro + " de oro";
        ActualizarMonedasOro();
    }
    public bool TengoOroParaConstruir()
    {
        for (int i = 0; i < MazoCartasDistrito.lstCartas.Count; i++)
        {
            if (MazoCartasDistrito.DameCarta(i).construida == false)
            {
                if (oro >= MazoCartasDistrito.DameCarta(i).carta.valor)
                {
                    if (ChequearDistritosIguales(MazoCartasDistrito.DameCarta(i))) { return true; }
                }
            }
        }
        return false;
    }

    public void Morir()
    {
        imgJugador[0].sharedMaterial = materiales[1];
        imgJugador[1].sharedMaterial = materiales[1];
        cartaPersonaje.BloquearCarta();
        muerto = true;
    }

    public void ChequearWin()
    {
        if (cartasConstruidas.Count >= 2)
        {
            ochoDistritosConstruidos = true;
        }
    }
    public void SumarPuntosDistritos()
    {
        puntos = 0;
        puntosBonus = "";
        foreach (Carta c in cartasConstruidas)
        {
            if (Tiene4distritosDistintos())
            {
                puntos += 3;
                puntosBonus += " + " + 3 ;
            }
            puntos += c.carta.valor;
        }
    }
    public bool Tiene4distritosDistintos()
    {
        bool azul = false;
        bool rojo = false;
        bool amarillo = false;
        bool verde = false;
        foreach (Carta c in cartasConstruidas)
        {
            switch (c.colorIndex)
            {
                case 1:
                    azul = true;
                    break;
                case 2:
                    rojo = true;
                    break;
                case 3:
                    amarillo = true;
                    break;
                case 4:
                    verde = true;
                    break;
            }
        }
        if(azul && rojo && amarillo && verde)
        {
            return true;
        }
        return false;
    }

    public string MostrarPuntosDistritos()
    {
        for (int i = 0; i < cartasConstruidas.Count; i++)
        {
            if(i == cartasConstruidas.Count - 1)
            {
                puntosDistritos += "" + cartasConstruidas[i].carta.valor;
            }
            else
            {
                puntosDistritos += "" + cartasConstruidas[i].carta.valor + " + ";
            }
        }      
        return puntosDistritos;
    }
}
public class ComparadorValor : IComparer<Jugador>
{
    public int Compare(Jugador x, Jugador y)
    {
        return y.cartaPersonaje.carta.valor.CompareTo(x.cartaPersonaje.carta.valor);
    }
}
public class ComparadorPuntos : IComparer<Jugador>
{
    public int Compare(Jugador x, Jugador y)
    {
        return y.puntos.CompareTo(x.puntos);
    }
}
