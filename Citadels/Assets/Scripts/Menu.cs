using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string filename = "";
    public static Action startGame;
    public GameObject SeleccionCorona;

    private void Awake()
    {

        filename = Application.dataPath + "/ReglasCitadels.pdf";
        Debug.Log(filename);
    }
    public void MostrarReglas()
    {
        Application.OpenURL(filename);
    }

    public void IniciarJuego()
    {
        startGame();
        StartCoroutine("StartGame");
    }
    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(2);
        SeleccionCorona.SetActive(true);
        this.gameObject.SetActive(false);
    }

}
