using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject objUiJuego;
    public GameObject objAsignacionCorona;

    public Button btnTerminarTurnoSeleccion;  // usa metodo terminar turno en el GM
    public Button btnIniciarSeleccion; // usa metodo iniciar juego en el GM
    public Button btnMostrarCartasPersonaje;   // usa metodo mostrarCartas en el GM
    public Button btnIniciarRonda;   // usa metodo definir turnos ronda en el GM
    public Button btnTerminarTurnoRonda;


    public bool pjSinHabilidad;

    public Image[] img_Jugador;
    public TMP_Text[] puntosDistritos;
    public TMP_Text[] puntosTotales;
    public TMP_Text[] puntosBonus;
    public Image img_Ganador;

    public TMP_Text[] puntosJugadores; // se muestra al final de cada ronda 

    [SerializeField]
    GameObject TablaDePuntos;


    #region Botones Ronda
    public GameObject btnsFase1;

    public GameObject btnsFase2;

    public Button btnUsarHabilidad;
    public Button btnConstruir;

    #endregion

    public TMP_Text txtIndicaciones;
    public TMP_Text txtAcciones;

    public static UIManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void ResetearUi()
    {
        objAsignacionCorona.SetActive(true);
        objUiJuego.SetActive(false);
        TablaDePuntos.SetActive(false);
    }
    public void DesactivarBtnsCorona()
    {
        objAsignacionCorona.SetActive(false);
        ActivarBtnIniciarSeleccion();
    }

    public void ActivarBtnIniciarSeleccion()
    {
        btnIniciarSeleccion.gameObject.SetActive(true);
        objUiJuego.SetActive(true);
    }
    public void ActivarBotonesSeleccion()
    {
        btnMostrarCartasPersonaje.gameObject.SetActive(true);
        btnTerminarTurnoSeleccion.gameObject.SetActive(true);
        btnTerminarTurnoSeleccion.interactable = false;
    }
    public void DesactivarBotonesSeleccion()
    {
        btnMostrarCartasPersonaje.gameObject.SetActive(false);
        btnTerminarTurnoSeleccion.gameObject.SetActive(false);
    }


    public void ActivarBtnsFase1()
    {
        btnsFase1.SetActive(true);
    } // agarrar oro o carta
    public void DesactivarBtnsFase1()
    {
        btnsFase1.SetActive(false);
    }

    public void ActivarBtnsFase2()
    {
        if(pjSinHabilidad == true) 
        {
            ActivarBtnConstruccion();
        }
        else
        {
            btnsFase2.SetActive(true);
        }
    } // construir o Habilidad
    public void DesactivarBtnsFase2()
    {
        btnsFase2.SetActive(false);
        btnConstruir.gameObject.SetActive(false);
    }

    public void ActivarBtnConstruccion() // El que haya quedado de la fase 2
    {
        btnConstruir.gameObject.SetActive(true);
    }
    public void DesactivarBtnConstruccion() 
    {
        btnConstruir.gameObject.SetActive(false);
    }
    public void ActivarBtnUsarHabilidad()
    {
        btnUsarHabilidad.gameObject.SetActive(true);
    }
    public void DesactivarBtnUsarHabilidad()
    {
        btnUsarHabilidad.gameObject.SetActive(false);
    }

    public void MostrarScores(List<Jugador> jugadores)
    {
        TablaDePuntos.SetActive(true);

        for (int i = 0; i < jugadores.Count; i++)
        {
            img_Jugador[i].sprite = jugadores[i].imgJugador[0].sprite;
            puntosBonus[i].text = jugadores[i].puntosBonus;
            puntosTotales[i].text = jugadores[i].puntos.ToString();
            puntosDistritos[i].text = jugadores[i].MostrarPuntosDistritos();
        }
        img_Ganador.sprite = jugadores[0].imgJugador[0].sprite;
    }
    public void SalirJuego()
    {
        Application.Quit();
    }
}
